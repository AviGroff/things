import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { handleNameChange, reset } from "./features/thing/newThingSlice";
import { useCreateThingMutation } from "./services/things";

const NewThing = () => {
    const dispatch = useDispatch()
    const [createThing] = useCreateThingMutation()
    const newThing = useSelector((state) => state.newThing);

    return (
        <form onSubmit={(e) => {
            e.preventDefault()
            createThing(newThing)
            dispatch(reset())
        }}>
            <div className="mb-3">
                <label htmlFor="name-field" className="form-label">Name</label>
                <input
                    type="text"
                    className="form-control"
                    id="name-field"
                    placeholder="Thing 1"
                    tabIndex={1}
                    value={newThing.name}
                    onChange={(e) => {
                        dispatch(handleNameChange(e.target.value))
                    }}
                />
            </div>
            <button className="btn btn-success">Submit</button>
            {` `}
            <button className="btn btn-info" onClick={(e) => {
                e.preventDefault()
                dispatch(reset())
            }}>Reset</button>
        </form>
    )
}

export default NewThing;
