from fastapi import APIRouter, Depends
from models import Thing, ThingParams, ThingsList
from queries.things import ThingsQueries

router = APIRouter()


@router.post('/api/things', response_model=Thing)
def create_thing(
    params: ThingParams,
    repo: ThingsQueries = Depends()
):
    return repo.create(params)


@router.get('/api/things', response_model=ThingsList)
def get_all_things(
    repo: ThingsQueries = Depends()
):
    return {
        'things': repo.get_all()
    }


@router.delete('/api/things/{thing_id}', response_model=bool)
def delete_thing(
    thing_id: str,
    repo: ThingsQueries = Depends()
):
    return repo.delete(thing_id)
